# Whitespace Image Display Test
Used to test image display in markdown files with whitespace in path.

## image with whitespace.jpg in root folder
![](image with whitespace.jpg)

## image_without_whitespace.jpg in root folder
![](image_without_whitespace.jpg)
